/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.testproject.dao;


import com.mycompany.testproject.helper.DatabaseHelper;
import com.mycompany.testproject.model.Receipt;
import com.mycompany.testproject.model.ReceiptItem;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author acer
 */
public class ReceiptDao implements Dao<Receipt>{

    @Override
    public Receipt get(int id) {
        Receipt item = null;
        String sql = "SELECT * FROM Receipt WHERE rcp_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Receipt.fromRS(rs);
             
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<Receipt> getAll() {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt item = Receipt.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Receipt save(Receipt obj) {
        ReceiptItemDao receiptItemDao = new ReceiptItemDao();
        String sql = "INSERT INTO receipt (rcp_date, rcp_cash, rcp_change, rcp_total, emp_name)" + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date = sdf.format(obj.getDate());
            stmt.setString(1, date);
            stmt.setDouble(2, obj.getCash());
            //System.out.println(date);
        //  String tempDate = "2022-10-15 15:20:15";
            stmt.setDouble(3, obj.getChange());
            stmt.setDouble(4, obj.getTotal());
            stmt.setString(5, obj.getEmp_name());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            //Add detail
            obj.setId(id);
            for(ReceiptItem od: obj.getReceiptItem()){
                receiptItemDao.save(od);
            }
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Receipt update (Receipt obj) {
       String sql = "UPDATE  receipt"
                + " SET rcp_date = ?,  rcp_cash= ?, rcp_change= ?,  rcp_total= ?,  emp_name=?"
                + " WHERE rcp_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDate(1, (Date) obj.getDate());
            stmt.setDouble(2, obj.getCash());
            stmt.setDouble(3, obj.getChange());
            stmt.setDouble(4, obj.getTotal());
            stmt.setString(5, obj.getEmp_name());
            stmt.setInt(6, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }    
    }

    @Override
    public int delete(Receipt obj) {
        String sql = "DELETE FROM receipt WHERE rcp_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Receipt> getAll(String where, String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt item = Receipt.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Receipt> getAll(String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM Receipt  ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt item = Receipt.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
}
