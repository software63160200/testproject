/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.testproject.dao;


import com.mycompany.testproject.helper.DatabaseHelper;
import com.mycompany.testproject.model.StockItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author acer
 */
public class StockItemDao implements Dao<StockItem>{

    @Override
    public StockItem get(int id) {
        StockItem stockItme = null;
        String sql = "SELECT * FROM StockItem WHERE stitem_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                stockItme = StockItem.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return  stockItme ;
    }

    @Override
    public List<StockItem> getAll() {
       ArrayList<StockItem> list = new ArrayList();
            String sql = "SELECT * FROM Stock_items";
            Connection conn = DatabaseHelper.getConnect();
        try
        {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next())
            {
                StockItem item = StockItem.fromRS(rs);
                list.add(item);

            }
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public StockItem save(StockItem obj) {
      String sql ="INSERT INTO Stock_items (stitem_product_name,stitem_price,stitem_amount,stock_id,mt_id)"
                + "VALUES(?, ?, ?, ?, ?)";
       Connection conn = DatabaseHelper.getConnect();
       try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getProductName());
            stmt.setDouble(2, obj.getPrice());
            stmt.setInt(3, obj.getProductAmount());
            stmt.setInt(4, obj.getStock().getId());
            stmt.setInt(5, obj.getMeterial().getId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;

    }

    @Override
    public StockItem update(StockItem obj) {
      String sql = "UPDATE Stock_items"
                + " SET stitem_product_name = ?, stitem_price= ?,stitem_amount= ?, stock_id = ?, mt_id=?"
                + " WHERE stitem_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getProductName());
            stmt.setDouble(2, obj.getPrice());
            stmt.setInt(3, obj.getProductAmount());
            stmt.setInt(4, obj.getStock().getId());
            stmt.setInt(5, obj.getMeterial().getId());
            stmt.setInt(6, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(StockItem obj) {
      String sql = "DELETE FROM Stock_items WHERE stitem_id=?";
        Connection conn = DatabaseHelper.getConnect();
        
        try
        {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<StockItem> getAll(String where, String order) {
     ArrayList<StockItem> list = new ArrayList();
        String sql = "SELECT * FROM Stock_items";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockItem stockitem = StockItem.fromRS(rs);
                list.add(stockitem);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<StockItem> getAll(String order) {
            ArrayList<StockItem> list = new ArrayList();
        String sql = "SELECT * FROM Stock_items ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockItem stockitem = StockItem.fromRS(rs);
                list.add(stockitem);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    
}
