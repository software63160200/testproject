/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.testproject.dao;

import com.mycompany.testproject.helper.DatabaseHelper;
import com.mycompany.testproject.model.Meterial;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author acer
 */
public class MeterialDao implements Dao<Meterial>{

    @Override
    public Meterial get(int id) {
        Meterial meterial = null;
        String sql = "SELECT * FROM Meterial WHERE mt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                meterial = Meterial.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return  meterial ;
    }

    @Override
    public List<Meterial> getAll() {
        ArrayList<Meterial> list = new ArrayList();
            String sql = "SELECT * FROM Meterial";
            Connection conn = DatabaseHelper.getConnect();
        try
        {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next())
            {
                Meterial item = Meterial.fromRS(rs);
                list.add(item);

            }
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Meterial save(Meterial obj) {
        String sql = "INSERT INTO Meteral (mt_product_name, mt_product_unit, mt_amount_in,mt_amount_out, mt_minimum_amout,mt_amount_balance)"
                + "VALUES(?, ?, ?, ?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getProductName());
            stmt.setString(2, obj.getProductUnit());
            stmt.setInt(3, obj.getAmountIn());
            stmt.setInt(4, obj.getAmountOut());
            stmt.setInt(5, obj.getMinimumAmount());
            stmt.setInt(6, obj.getAmountBalance());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Meterial update(Meterial obj) {
        String sql = "UPDATE Meteral"
                + " SET mt_product_name = ?, mt_product_unit= ?,mt_amount_in= ?, mt_amount_out = ?, mt_minimum_amout=?,mt_amount_balance=?"
                + " WHERE mt_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getProductName());
            stmt.setString(2, obj.getProductUnit());
            stmt.setInt(3, obj.getAmountIn());
            stmt.setInt(4, obj.getAmountOut());
            stmt.setInt(5, obj.getMinimumAmount());
            stmt.setInt(6, obj.getAmountBalance());
            stmt.setInt(7, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Meterial obj) {
       String sql = "DELETE FROM Meterial WHERE mt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        
        try
        {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Meterial> getAll(String where, String order) {
            ArrayList<Meterial> list = new ArrayList();
        String sql = "SELECT * FROM Meterial";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Meterial meterial = Meterial.fromRS(rs);
                list.add(meterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Meterial> getAll(String order) {
            ArrayList<Meterial> list = new ArrayList();
        String sql = "SELECT * FROM Meterial ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Meterial meterial = Meterial.fromRS(rs);
                list.add(meterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    
}
