/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.testproject.dao;

import com.mycompany.testproject.helper.DatabaseHelper;
import com.mycompany.testproject.model.Stock;
import com.mycompany.testproject.model.StockItem;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author fewsp
 */
public class StockDao implements Dao<Stock>{

    @Override
    public Stock get(int id) {
        Stock stock = null;
        String sql = "SELECT * FROM Stock WHERE stock_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                stock = Stock.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return  stock ;

    }

    @Override
    public ArrayList<Stock> getAll() {
        ArrayList<Stock> list = new ArrayList();
            String sql = "SELECT * FROM Stock";
            Connection conn = DatabaseHelper.getConnect();
        try
        {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next())
            {
                Stock item = Stock.fromRS(rs);
                list.add(item);

            }
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Stock save(Stock obj) {
        StockItemDao stockItemDao =new StockItemDao();
        String sql = "INSERT INTO Stock (stock_date,stock_emp_name)" + "VALUES(?, ?)";
        Connection conn = DatabaseHelper.getConnect();
           try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            
          SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
          String date = sdf.format(obj.getDate());
            //System.out.println(date);
        //  String tempDate = "2022-10-15 15:20:15";
            stmt.setString(1,date);
            stmt.setString(2, obj.getEmployee());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            //Add detail
            obj.setId(id);
            for(StockItem st : obj.getStockDetails()){
                stockItemDao.save(st);
            }
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;

        }
        
    }

    @Override
    public Stock update(Stock obj) {
       String sql = "UPDATE Stock"
                + " SET stock_date = ?,stock_emp_name= ?"
                + " WHERE stock_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDate(1, (Date) obj.getDate());
            stmt.setString(2, obj.getEmployee());
            stmt.setInt(3, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Stock obj) {
        String sql = "DELETE FROM Stock WHERE stock_id=?";
        Connection conn = DatabaseHelper.getConnect();
        
        try
        {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Stock> getAll(String where, String order) {
          ArrayList<Stock> list = new ArrayList();
        String sql = "SELECT * FROM Stock";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Stock stock = Stock.fromRS(rs);
                list.add(stock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
     public List<Stock> getAll(String order) {
          ArrayList<Stock> list = new ArrayList();
        String sql = "SELECT * FROM Stock";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Stock stock = Stock.fromRS(rs);
                list.add(stock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    
    
}
