/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.testproject.service;

import com.mycompany.testproject.dao.ShoppingDao;
import com.mycompany.testproject.model.ReportShopping;
import java.util.List;

/**
 *
 * @author acer
 */
public class ReportShopService {

    public List<ReportShopping> getreportShopByDay() {
        ShoppingDao dao = new ShoppingDao();
        return dao.getDayShopReport();
    }

    public List<ReportShopping> getreportShopByMouth() {
        ShoppingDao dao = new ShoppingDao();
        return dao.getMonthShopReport();
    }
    
    public List<ReportShopping> getreportShopByYear() {
        ShoppingDao dao = new ShoppingDao();
        return dao.getYearShopReport();
    }

}
