/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.testproject.service;

import com.mycompany.testproject.dao.MeterialDao;
import com.mycompany.testproject.model.Meterial;
import java.util.List;

/**
 *
 * @author acer
 */
public class MeterialService {
    public List<Meterial> getMet() {
        MeterialDao metDao = new MeterialDao();
        return metDao.getAll("mt_product_name asc");
        
    }

    public Meterial addNew(Meterial editedMet) {
       MeterialDao metDao = new MeterialDao();
        return metDao.save(editedMet);
    }
    
    public Meterial update(Meterial editedMet) {
       MeterialDao metDao = new MeterialDao();
        return metDao.update(editedMet);
    }

    public int delete(Meterial editedMet) {
       MeterialDao metDao = new MeterialDao();
        return metDao.delete(editedMet);
    }
}
