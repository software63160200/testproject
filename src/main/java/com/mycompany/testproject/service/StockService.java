/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.testproject.service;

import com.mycompany.testproject.dao.StockItemDao;
import com.mycompany.testproject.model.StockItem;
import java.util.List;

/**
 *
 * @author acer
 */
public class StockService {
    public List<StockItem> getSt() {
        StockItemDao stitemDao = new StockItemDao();
        return stitemDao.getAll();
        
    }

    public StockItem addNew(StockItem editedSt) {
       StockItemDao stitemDao = new StockItemDao();
        return stitemDao.save(editedSt);
    }
    
    public StockItem update(StockItem editedSt) {
       StockItemDao stitemDao = new StockItemDao();
        return stitemDao.update(editedSt);
    }

    public int delete(StockItem editedSt) {
   StockItemDao stitemDao = new StockItemDao();
        return stitemDao.delete(editedSt);
    }
}
