/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.testproject.service;

import com.mycompany.testproject.dao.ReceiptItemDao;
import com.mycompany.testproject.model.ReceiptItem;
import java.util.List;

/**
 *
 * @author acer
 */
public class ReceiptService {
     public List<ReceiptItem> getRec() {
        ReceiptItemDao rectiemDao = new ReceiptItemDao();
        return rectiemDao.getAll();
        
    }

    public ReceiptItem addNew(ReceiptItem editedMet) {
       ReceiptItemDao metDao = new ReceiptItemDao();
        return metDao.save(editedMet);
    }
    
    public ReceiptItem update(ReceiptItem editedMet) {
       ReceiptItemDao rectiemDao = new ReceiptItemDao();
        return rectiemDao.update(editedMet);
    }

    public int delete(ReceiptItem editedMet) {
       ReceiptItemDao rectiemDao = new ReceiptItemDao();
        return rectiemDao.delete(editedMet);
    }
}
