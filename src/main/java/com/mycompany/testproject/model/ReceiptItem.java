/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.testproject.model;

import com.mycompany.testproject.dao.MeterialDao;
import com.mycompany.testproject.dao.ReceiptDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class ReceiptItem {

    private int id;
    private String ProductName; //ชื่อสินค้าที่ซื้อ
    private double price; // ราคาที่ซื้อ
    private int amount; // จำนวนที่ซื้อ
    private double total; //ราคารวม ราคา*จำนวน
    private Receipt receipt; // FK Receipt 
    private Meterial meterial;// FK Meterial

    public ReceiptItem(int id, String ProductName, double price, int amount, double total, Receipt receipt, Meterial meterial) {
        this.id = id;
        this.ProductName = ProductName;
        this.price = price;
        this.amount = amount;
        this.total = total;
        this.receipt = receipt;
        this.meterial = meterial;
    }

    public ReceiptItem(String ProductName, double price, int amount, double total, Receipt receipt, Meterial meterial) {
        this.id = -1;
        this.ProductName = ProductName;
        this.price = price;
        this.amount = amount;
        this.total = total;
        this.receipt = receipt;
        this.meterial = meterial;
    }

    public ReceiptItem() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    public Meterial getMeterial() {
        return meterial;
    }

    public void setMeterial(Meterial meterial) {
        this.meterial = meterial;
    }

    @Override
    public String toString() {
        return "Receipt_item{" + "id=" + id + ", ProductName=" + ProductName + ", price=" + price + ", amount=" + amount + ", total=" + total + ", receipt=" + receipt + ", meterial=" + meterial + '}';
    }

    public static ReceiptItem fromRS(ResultSet rs) {
        ReceiptItem rec_item = new ReceiptItem();
        ReceiptDao ReceiptDao =new ReceiptDao();
        MeterialDao MeterialDao =new MeterialDao();
        
        
        try {
            
            // PK Receipt_item
            rec_item.setId(rs.getInt("reitem_id"));

            // FK Receipt
            int ReceiptId = rs.getInt("rcp_id"); 
            Receipt receipt_item = ReceiptDao.get(ReceiptId); 
            rec_item.setReceipt(receipt_item);
            
            // FK Meterial
             int MetId = rs.getInt("mt_id"); 
             Meterial  met_item = MeterialDao.get(MetId); 
             rec_item.setMeterial(met_item);
            
            rec_item.setProductName(rs.getString("reitem_product_name"));
            rec_item.setPrice(rs.getDouble("reitem_price"));
            rec_item.setAmount(rs.getInt("reitem_amout"));
            rec_item.setTotal(rs.getDouble("reitem_total"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);

        }

        return rec_item;
    }

}
