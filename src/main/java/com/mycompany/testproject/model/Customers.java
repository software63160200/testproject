/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.testproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class Customers {

    private int id;
    private String name; // ชื่อลูกค้า
    private String status; //สถานะลูกค้า
    private String tel; //เบอร์โทรลูกค้า
    private int points; //คะเเนนสะสม

    public Customers(int id, String name, String status, String tel, int points) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.tel = tel;
        this.points = points;
    }

    public Customers(String name, String status, String tel, int points) {  // เพิ่ม ลูกค้าใหม่
        this.id = -1;
        this.name = name;
        this.status = status;
        this.tel = tel;
        this.points = points;
    }

    public Customers() { // เพิ่ม ลูกค้าใหม่
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "Customers{" + "id=" + id + ", name=" + name + ", status=" + status + ", tel=" + tel + ", points=" + points + '}';
    }
    
    public static Customers fromRS(ResultSet rs) {
        Customers customer = new Customers();
        try {
            customer.setId(rs.getInt("cus_id"));
             customer.setName(rs.getString("cus_name"));
            customer.setStatus(rs.getString("cus_status"));
            customer.setTel(rs.getString("cus_tel"));
            customer.setPoints(rs.getInt("cus_points"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);

        }

        return customer;
    }

}
